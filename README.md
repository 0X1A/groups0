# groups
[![Build Status](https://travis-ci.org/0X1A/groups.svg)](https://travis-ci.org/0X1A/groups)
[![](http://meritbadge.herokuapp.com/groups)](https://crates.io/crates/groups)

Parses the group database `/etc/group` to obtain information about groups

## [Documentation](https://albertocorona.com/doc/groups)
This crate is available on [crates.io](https://crates.io/crates/groups) and can
be used by adding the following to your `Cargo.toml` file:

```toml
[dependencies]
groups = "*"
```
